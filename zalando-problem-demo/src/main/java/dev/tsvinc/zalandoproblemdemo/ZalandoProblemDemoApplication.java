package dev.tsvinc.zalandoproblemdemo;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.DispatcherServlet;
import org.zalando.problem.ProblemModule;

@SpringBootApplication
public class ZalandoProblemDemoApplication {

  public static void main(String[] args) {
    ApplicationContext ctx = SpringApplication.run(ZalandoProblemDemoApplication.class, args);
    DispatcherServlet dispatcherServlet = (DispatcherServlet) ctx.getBean("dispatcherServlet");
    dispatcherServlet.setThrowExceptionIfNoHandlerFound(true);
  }

  @Bean
  public ObjectMapper objectMapper() {
    final var objectMapper = new ObjectMapper();
    objectMapper.registerModule(new ProblemModule());
    objectMapper.registerSubtypes(SimpleProblem.class);
    return objectMapper;
  }
}
