package dev.tsvinc.zalandoproblemdemo;

import java.util.Collections;
import java.util.List;
import javax.annotation.Nonnull;
import org.springframework.http.MediaType;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.accept.ContentNegotiationStrategy;
import org.springframework.web.context.request.NativeWebRequest;

final class FallbackContentNegotiationStrategy implements ContentNegotiationStrategy {

  private final List<MediaType> all = Collections.singletonList(MediaType.ALL);
  private final ContentNegotiationStrategy delegate;

  FallbackContentNegotiationStrategy(final ContentNegotiationStrategy delegate) {
    this.delegate = delegate;
  }

  @Override
  @Nonnull
  public List<MediaType> resolveMediaTypes(@Nonnull final NativeWebRequest request)
      throws HttpMediaTypeNotAcceptableException {
    final List<MediaType> mediaTypes = delegate.resolveMediaTypes(request);

    if (mediaTypes.isEmpty()) {
      return all;
    }

    return mediaTypes;
  }
}
