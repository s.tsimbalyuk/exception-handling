package dev.tsvinc.zalandoproblemdemo;

import java.net.URI;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

public final class SimpleProblem extends AbstractThrowableProblem {
  private static final URI TYPE = URI.create("https://example.org/not-found");

  public SimpleProblem(int taskId) {
    super(TYPE, "Not found", Status.I_AM_A_TEAPOT, String.format("Nope, got: %d", taskId));
  }
}
