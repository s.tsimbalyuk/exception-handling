package dev.tsvinc.zalandoproblemdemo;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/orders")
public class ProblemController {
  private final SimpleService simpleService;

  public ProblemController(SimpleService simpleService) {
    this.simpleService = simpleService;
  }

  @GetMapping(value = "/ok/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Ok> ok(@PathVariable("id") int id) {
    return ResponseEntity.ok(
        Ok.builder().description("ok").id(simpleService.someMethod(id)).build());
  }
}
