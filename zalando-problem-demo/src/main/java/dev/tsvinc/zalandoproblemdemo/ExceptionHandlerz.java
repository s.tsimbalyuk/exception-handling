package dev.tsvinc.zalandoproblemdemo;

import static javax.servlet.RequestDispatcher.ERROR_EXCEPTION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.context.request.RequestAttributes.SCOPE_REQUEST;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;
import javax.servlet.http.HttpServletResponse;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.accept.ContentNegotiationStrategy;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.NativeWebRequest;
import org.zalando.problem.Problem;
import org.zalando.problem.Status;

@ControllerAdvice
public class ExceptionHandlerz {

  public static final String PROBLEM_VALUE = "application/problem+json";
  public static final MediaType PROBLEM = MediaType.parseMediaType(PROBLEM_VALUE);
  public static final String X_PROBLEM_VALUE = "application/x.problem+json";
  public static final MediaType X_PROBLEM = MediaType.parseMediaType(X_PROBLEM_VALUE);
  private static final Logger LOG = LoggerFactory.getLogger(ExceptionHandlerz.class);

  public static void log(final Throwable throwable, final HttpStatus status) {
    if (status.is4xxClientError()) {
      LOG.warn("{}: {}", status.getReasonPhrase(), throwable.getMessage());
    } else if (status.is5xxServerError()) {
      LOG.error(status.getReasonPhrase(), throwable);
    }
  }

  public static Optional<MediaType> getProblemMediaType(final List<MediaType> mediaTypes) {
    for (final MediaType mediaType : mediaTypes) {
      if (mediaType.includes(APPLICATION_JSON) || mediaType.includes(PROBLEM)) {
        return Optional.of(PROBLEM);
      } else if (mediaType.includes(X_PROBLEM)) {
        return Optional.of(X_PROBLEM);
      }
    }

    return Optional.empty();
  }

  public static <T, X extends Throwable> ThrowingSupplier<T, X> throwingSupplier(
      final ThrowingSupplier<T, X> supplier) {
    return supplier;
  }

  public static ResponseEntity<Problem> fallback(final Problem problem, final HttpHeaders headers) {
    return ResponseEntity.status(
            HttpStatus.valueOf(
                Optional.ofNullable(problem.getStatus())
                    .orElse(Status.INTERNAL_SERVER_ERROR)
                    .getStatusCode()))
        .headers(headers)
        .contentType(PROBLEM)
        .body(problem);
  }

  @ExceptionHandler
  public ResponseEntity<Problem> handleNoSuchElementException(
      SimpleProblem ex, NativeWebRequest request) throws HttpMediaTypeNotAcceptableException {
    final var problem =
        Problem.builder()
            .withStatus(Status.I_AM_A_TEAPOT)
            .withTitle(ex.getTitle())
            .withDetail(ex.getDetail())
            .withType(URI.create("https://example.org/out-of-stock"))
            .build();
    return create(ex, problem, request, new HttpHeaders());
  }

  private ResponseEntity<Problem> create(
      final Throwable throwable,
      final Problem problem,
      final NativeWebRequest request,
      final HttpHeaders headers)
      throws HttpMediaTypeNotAcceptableException {
    final HttpStatus status =
        HttpStatus.valueOf(
            Optional.ofNullable(problem.getStatus())
                .orElse(Status.INTERNAL_SERVER_ERROR)
                .getStatusCode());

    log(throwable, status);

    if (status == HttpStatus.INTERNAL_SERVER_ERROR) {
      request.setAttribute(ERROR_EXCEPTION, throwable, SCOPE_REQUEST);
    }

    return process(
        negotiate(request)
            .map(
                contentType ->
                    ResponseEntity.status(status)
                        .headers(headers)
                        .contentType(contentType)
                        .body(problem))
            .orElseGet(
                throwingSupplier(
                    () -> {
                      final ResponseEntity<Problem> fallback = fallback(problem, headers);

                      if (fallback.getBody() == null) {
                        /*
                         * Ugly hack to workaround an issue with Tomcat and Spring as described in
                         * https://github.com/zalando/problem-spring-web/issues/84.
                         *
                         * The default fallback in case content negotiation failed is a 406 Not Acceptable without
                         * a body. Tomcat will then display its error page since no body was written and the response
                         * was not committed. In order to force Spring to flush/commit one would need to provide a
                         * body but that in turn would fail because Spring would then fail to negotiate the correct
                         * content type.
                         *
                         * Writing the status code, headers and flushing the body manually is a dirty way to bypass
                         * both parties, Tomcat and Spring, at the same time.
                         */
                        final ServerHttpResponse response =
                            new ServletServerHttpResponse(
                                request.getNativeResponse(HttpServletResponse.class));

                        response.setStatusCode(fallback.getStatusCode());
                        response.getHeaders().putAll(fallback.getHeaders());
                        response.getBody(); // just so we're actually flushing the body...
                        response.flush();
                      }

                      return fallback;
                    })),
        request);
  }

  private Optional<MediaType> negotiate(final NativeWebRequest request)
      throws HttpMediaTypeNotAcceptableException {
    final ContentNegotiationStrategy negotiator = ContentNegotiation.DEFAULT;
    final List<MediaType> mediaTypes = negotiator.resolveMediaTypes(request);
    return getProblemMediaType(mediaTypes);
  }

  ResponseEntity<Problem> process(
      final ResponseEntity<Problem> entity,
      @SuppressWarnings("UnusedParameters") final NativeWebRequest request) {
    return process(entity);
  }

  ResponseEntity<Problem> process(final ResponseEntity<Problem> entity) {
    return entity;
  }

  @FunctionalInterface
  public interface ThrowingSupplier<T, X extends Throwable> extends Supplier<T> {

    T tryGet() throws X;

    @Override
    @SneakyThrows
    default T get() {
      return tryGet();
    }
  }
}
