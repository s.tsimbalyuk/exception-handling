package dev.tsvinc.zalandoproblemdemo;

import org.springframework.stereotype.Service;

@Service
public class SimpleService {
  public int someMethod(int i) {
    if (i == 5) {
      return i;
    } else {
      throw new SimpleProblem(i);
    }
  }
}
