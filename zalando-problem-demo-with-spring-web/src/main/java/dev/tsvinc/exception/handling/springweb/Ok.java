package dev.tsvinc.exception.handling.springweb;

public class Ok {
  private int id;
  private String description;

  public Ok(int id, String description) {
    this.id = id;
    this.description = description;
  }

  public Ok() {}

  public static OkBuilder builder() {
    return new OkBuilder();
  }

  public int getId() {
    return this.id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getDescription() {
    return this.description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public boolean equals(final Object o) {
    if (o == this) {
      return true;
    }
    if (!(o instanceof Ok)) {
      return false;
    }
    final Ok other = (Ok) o;
    if (!other.canEqual(this)) {
      return false;
    }
    if (this.getId() != other.getId()) {
      return false;
    }
    final Object o1Description = this.getDescription();
    final Object o2Description = other.getDescription();
    return o1Description == null ? o2Description == null : o1Description.equals(o2Description);
  }

  protected boolean canEqual(final Object other) {
    return other instanceof Ok;
  }

  public int hashCode() {
    final int PRIME = 59;
    int result = 1;
    result = result * PRIME + this.getId();
    final Object o1Description = this.getDescription();
    result = result * PRIME + (o1Description == null ? 43 : o1Description.hashCode());
    return result;
  }

  public String toString() {
    return "Ok(id=" + this.getId() + ", description=" + this.getDescription() + ")";
  }

  public static class OkBuilder {

    private int id;
    private String description;

    OkBuilder() {}

    public Ok.OkBuilder id(int id) {
      this.id = id;
      return this;
    }

    public Ok.OkBuilder description(String description) {
      this.description = description;
      return this;
    }

    public Ok build() {
      return new Ok(id, description);
    }

    public String toString() {
      return "Ok.OkBuilder(id=" + this.id + ", description=" + this.description + ")";
    }
  }
}
