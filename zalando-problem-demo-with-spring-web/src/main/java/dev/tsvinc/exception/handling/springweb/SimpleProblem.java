package dev.tsvinc.exception.handling.springweb;

public class SimpleProblem extends RuntimeException {

  private final String message;
  private final String type;
  private final String detail;
  private final String status;

  public SimpleProblem(String message, String type, String detail, String status) {
    super(message);
    this.message = message;
    this.type = type;
    this.detail = detail;
    this.status = status;
  }

  public static SimpleProblemBuilder builder() {
    return new SimpleProblemBuilder();
  }

  @Override
  public String getMessage() {
    return message;
  }

  public String getType() {
    return type;
  }

  public String getDetail() {
    return detail;
  }

  public String getStatus() {
    return status;
  }

  public static class SimpleProblemBuilder {

    private String message;
    private String type;
    private String detail;
    private String status;

    SimpleProblemBuilder() {}

    public SimpleProblemBuilder message(String message) {
      this.message = message;
      return this;
    }

    public SimpleProblemBuilder type(String type) {
      this.type = type;
      return this;
    }

    public SimpleProblemBuilder detail(String detail) {
      this.detail = detail;
      return this;
    }

    public SimpleProblemBuilder status(String status) {
      this.status = status;
      return this;
    }

    public SimpleProblem build() {
      return new SimpleProblem(message, type, detail, status);
    }

    public String toString() {
      return "SimpleProblem.SimpleProblemBuilder(message="
          + this.message
          + ", type="
          + this.type
          + ", detail="
          + this.detail
          + ", status="
          + this.status
          + ")";
    }
  }
}
