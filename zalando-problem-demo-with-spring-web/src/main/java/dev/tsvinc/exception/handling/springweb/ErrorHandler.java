package dev.tsvinc.exception.handling.springweb;

import java.net.URI;
import java.util.Optional;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.zalando.problem.Problem;
import org.zalando.problem.Status;
import org.zalando.problem.spring.web.advice.ProblemHandling;

@ControllerAdvice
public class ErrorHandler implements ProblemHandling {

  @ExceptionHandler(SimpleProblem.class)
  public ResponseEntity<Problem> handle(SimpleProblem simple) {
    return ResponseEntity.of(
        Optional.of(
            Problem.builder()
                .withType(URI.create(simple.getType()))
                .withTitle(simple.getMessage())
                .withDetail(simple.getDetail())
                .withStatus(Status.valueOf(simple.getStatus()))
                .build()));
  }

  @Override
  public boolean isCausalChainsEnabled() {
    return true;
  }
}
