package dev.tsvinc.simple.problem.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
class Controller {

  @GetMapping(path = "/orders/{id}")
  public String demo(@PathVariable("id") String id) {
    throw new SomeSpecificException(id);
  }
}
