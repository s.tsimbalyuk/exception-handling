package dev.tsvinc.simple.problem.demo;

class SomeSpecificException extends RuntimeException {

  public SomeSpecificException(String message) {
    super(message);
  }
}
