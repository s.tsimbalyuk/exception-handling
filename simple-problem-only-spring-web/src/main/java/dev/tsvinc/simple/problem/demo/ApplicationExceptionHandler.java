package dev.tsvinc.simple.problem.demo;

import java.net.URI;
import java.util.Optional;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.zalando.problem.Problem;
import org.zalando.problem.Status;
import org.zalando.problem.spring.web.advice.ProblemHandling;

@ControllerAdvice
class ApplicationExceptionHandler implements ProblemHandling {

  @ExceptionHandler(SomeSpecificException.class)
  public ResponseEntity<Problem> handleDemoNotWorkingException(SomeSpecificException exception) {
    return ResponseEntity.of(
        Optional.of(
            Problem.builder()
                .withType(URI.create("https://mytodolistapp.com/probs/cant-move-item-to-list"))
                .withTitle("The target list is at maximum capacity")
                .withDetail(exception.getMessage())
                .withStatus(Status.FORBIDDEN)
                .build()));
  }
}
