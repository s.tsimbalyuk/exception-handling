package dev.tsvinc.exception.handling;

public class RandomException extends BaseException {

  public RandomException(final String message) {
    super(message);
  }

  public RandomException(final String message, final Throwable cause) {
    super(message, cause);
  }
}
