package dev.tsvinc.exception.handling;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class ErrorsHandler {

  @ExceptionHandler(RandomException.class)
  @ResponseStatus(HttpStatus.I_AM_A_TEAPOT)
  @ResponseBody
  public GeneralResponse handle(RandomException e, WebRequest request) {
    return GeneralResponse.builder()
        .detail(e.getMessage())
        .status(String.valueOf(HttpStatus.I_AM_A_TEAPOT.value()))
        .title("Not found")
        .type(request.getContextPath())
        .build();
  }
}
