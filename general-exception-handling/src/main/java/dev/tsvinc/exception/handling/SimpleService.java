package dev.tsvinc.exception.handling;

import java.io.FileNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class SimpleService {
  public int someMethod(int i) throws FileNotFoundException {
    if (i == 5) {
      return i;
    } else if (i == 6) {
      throw new RandomException("Nope, got: " + i);
    } else {
      throw new FileNotFoundException("welp");
    }
  }
}
