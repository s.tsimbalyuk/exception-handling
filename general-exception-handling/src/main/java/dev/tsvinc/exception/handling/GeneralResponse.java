package dev.tsvinc.exception.handling;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class GeneralResponse {
  private String type;
  private String title;
  private String status;
  private String detail;
  private String extraFieldOne;
  private String extraFieldTwo;
}
